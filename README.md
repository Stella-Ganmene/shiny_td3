# Shiny TD 3 : Connexion à une base de données

Mettre à jour le TD1 en se utilisant la base de données *sejour* (plutôt que les cvs) pour récupérer les tables **sejour** et **hopital**. Proposer un filtre pour sélectionner un hôpital et afficher les séjours correspondants.

## Résultat attendu 

![](https://gitlab.com/d8096/cours_data_science/data_product/shiny/shiny_td3_correction/-/raw/main/resultat.png){width=500px}
